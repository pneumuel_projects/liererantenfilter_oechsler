﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angebotsliste
{
    class Lieferant
    {
        private string filename;
        private string name;
        private int hits;

        private bool check11 = false;
        private bool check12 = false;
        private bool check13 = false;
        private bool check14 = false;
        private bool check15 = false;
        private bool check16 = false;
        private bool check17 = false;

        private bool check21 = false;
        private bool check22 = false;
        private bool check23 = false;
        private bool check24 = false;
        private bool check25 = false;
        private bool check26 = false;

        private bool check31 = false;
        private bool check32 = false;
        private bool check33 = false;
        private bool check34 = false;
        private bool check35 = false;
        private bool check36 = false;
        private bool check37 = false;
        private bool check38 = false;

        private bool check41 = false;
        private bool check42 = false;
        private bool check43 = false;
        private bool check44 = false;
        private bool check45 = false;

        private bool check51 = false;
        private bool check52 = false;
        private bool check53 = false;
        private bool check54 = false;
        private bool check55 = false;
        private bool check56 = false;
        private bool check57 = false;
        private bool check58 = false;

        private bool check61 = false;
        private bool check62 = false;
        private bool check63 = false;
        private bool check64 = false;
        private bool check65 = false;
        private bool check66 = false;
        private bool check67 = false;


        public int Hits { get => hits; set => hits = value; }
        public string Name { get => name; set => name = value; }
        public bool Check11 { get => check11; set => check11 = value; }
        public bool Check12 { get => check12; set => check12 = value; }
        public bool Check13 { get => check13; set => check13 = value; }
        public bool Check14 { get => check14; set => check14 = value; }
        public bool Check15 { get => check15; set => check15 = value; }
        public bool Check16 { get => check16; set => check16 = value; }
        public bool Check17 { get => check17; set => check17 = value; }
        public bool Check21 { get => check21; set => check21 = value; }
        public bool Check22 { get => check22; set => check22 = value; }
        public bool Check23 { get => check23; set => check23 = value; }
        public bool Check24 { get => check24; set => check24 = value; }
        public bool Check25 { get => check25; set => check25 = value; }
        public bool Check26 { get => check26; set => check26 = value; }
        public bool Check31 { get => check31; set => check31 = value; }
        public bool Check32 { get => check32; set => check32 = value; }
        public bool Check33 { get => check33; set => check33 = value; }
        public bool Check34 { get => check34; set => check34 = value; }
        public bool Check35 { get => check35; set => check35 = value; }
        public bool Check36 { get => check36; set => check36 = value; }
        public bool Check37 { get => check37; set => check37 = value; }
        public bool Check38 { get => check38; set => check38 = value; }
        public bool Check41 { get => check41; set => check41 = value; }
        public bool Check42 { get => check42; set => check42 = value; }
        public bool Check43 { get => check43; set => check43 = value; }
        public bool Check44 { get => check44; set => check44 = value; }
        public bool Check45 { get => check45; set => check45 = value; }
        public bool Check51 { get => check51; set => check51 = value; }
        public bool Check52 { get => check52; set => check52 = value; }
        public bool Check53 { get => check53; set => check53 = value; }
        public bool Check54 { get => check54; set => check54 = value; }
        public bool Check55 { get => check55; set => check55 = value; }
        public bool Check56 { get => check56; set => check56 = value; }
        public bool Check57 { get => check57; set => check57 = value; }
        public bool Check58 { get => check58; set => check58 = value; }
        public bool Check61 { get => check61; set => check61 = value; }
        public bool Check62 { get => check62; set => check62 = value; }
        public bool Check63 { get => check63; set => check63 = value; }
        public bool Check64 { get => check64; set => check64 = value; }
        public bool Check65 { get => check65; set => check65 = value; }
        public bool Check66 { get => check66; set => check66 = value; }
        public bool Check67 { get => check67; set => check67 = value; }
        public string Filename { get => filename; set => filename = value; }
    }
}
