﻿namespace Angebotsliste
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnFolder = new System.Windows.Forms.Button();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.check11 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.check12 = new System.Windows.Forms.CheckBox();
            this.check13 = new System.Windows.Forms.CheckBox();
            this.check14 = new System.Windows.Forms.CheckBox();
            this.check15 = new System.Windows.Forms.CheckBox();
            this.check16 = new System.Windows.Forms.CheckBox();
            this.check17 = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.check67 = new System.Windows.Forms.CheckBox();
            this.check66 = new System.Windows.Forms.CheckBox();
            this.check65 = new System.Windows.Forms.CheckBox();
            this.check64 = new System.Windows.Forms.CheckBox();
            this.check63 = new System.Windows.Forms.CheckBox();
            this.check62 = new System.Windows.Forms.CheckBox();
            this.check61 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.check58 = new System.Windows.Forms.CheckBox();
            this.check57 = new System.Windows.Forms.CheckBox();
            this.check56 = new System.Windows.Forms.CheckBox();
            this.check55 = new System.Windows.Forms.CheckBox();
            this.check54 = new System.Windows.Forms.CheckBox();
            this.check53 = new System.Windows.Forms.CheckBox();
            this.check52 = new System.Windows.Forms.CheckBox();
            this.check51 = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.check45 = new System.Windows.Forms.CheckBox();
            this.check44 = new System.Windows.Forms.CheckBox();
            this.check43 = new System.Windows.Forms.CheckBox();
            this.check42 = new System.Windows.Forms.CheckBox();
            this.check41 = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.check38 = new System.Windows.Forms.CheckBox();
            this.check37 = new System.Windows.Forms.CheckBox();
            this.check36 = new System.Windows.Forms.CheckBox();
            this.check35 = new System.Windows.Forms.CheckBox();
            this.check34 = new System.Windows.Forms.CheckBox();
            this.check33 = new System.Windows.Forms.CheckBox();
            this.check32 = new System.Windows.Forms.CheckBox();
            this.check31 = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.check26 = new System.Windows.Forms.CheckBox();
            this.check25 = new System.Windows.Forms.CheckBox();
            this.check24 = new System.Windows.Forms.CheckBox();
            this.check23 = new System.Windows.Forms.CheckBox();
            this.check22 = new System.Windows.Forms.CheckBox();
            this.check21 = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnFilter = new System.Windows.Forms.Button();
            this.progressFilter = new System.Windows.Forms.ProgressBar();
            this.listBox = new System.Windows.Forms.ListBox();
            this.listHits = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnFolder
            // 
            this.btnFolder.Location = new System.Drawing.Point(788, 82);
            this.btnFolder.Name = "btnFolder";
            this.btnFolder.Size = new System.Drawing.Size(75, 23);
            this.btnFolder.TabIndex = 0;
            this.btnFolder.Text = "Auswahl";
            this.btnFolder.UseVisualStyleBackColor = true;
            this.btnFolder.Click += new System.EventHandler(this.BtnFolder_Click);
            // 
            // txtFileName
            // 
            this.txtFileName.Location = new System.Drawing.Point(59, 85);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(723, 20);
            this.txtFileName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Auswahl Ordner mit Lieferantendaten";
            // 
            // check11
            // 
            this.check11.AutoSize = true;
            this.check11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check11.Location = new System.Drawing.Point(6, 34);
            this.check11.Name = "check11";
            this.check11.Size = new System.Drawing.Size(255, 24);
            this.check11.TabIndex = 3;
            this.check11.Text = "Urformen aus flüssigen Zustand";
            this.check11.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 29);
            this.label2.TabIndex = 4;
            this.label2.Text = "Urformen";
            // 
            // check12
            // 
            this.check12.AutoSize = true;
            this.check12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check12.Location = new System.Drawing.Point(6, 64);
            this.check12.Name = "check12";
            this.check12.Size = new System.Drawing.Size(299, 24);
            this.check12.TabIndex = 5;
            this.check12.Text = "Urformen aus dem platischen Zustand";
            this.check12.UseVisualStyleBackColor = true;
            // 
            // check13
            // 
            this.check13.AutoSize = true;
            this.check13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check13.Location = new System.Drawing.Point(6, 94);
            this.check13.Name = "check13";
            this.check13.Size = new System.Drawing.Size(287, 24);
            this.check13.TabIndex = 6;
            this.check13.Text = "Urformen aus dem breiigen Zustand ";
            this.check13.UseVisualStyleBackColor = true;
            // 
            // check14
            // 
            this.check14.AutoSize = true;
            this.check14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check14.Location = new System.Drawing.Point(6, 124);
            this.check14.Name = "check14";
            this.check14.Size = new System.Drawing.Size(427, 24);
            this.check14.TabIndex = 7;
            this.check14.Text = "Urfomen aus dem körnigen oder pulverförmigen Zustand";
            this.check14.UseVisualStyleBackColor = true;
            // 
            // check15
            // 
            this.check15.AutoSize = true;
            this.check15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check15.Location = new System.Drawing.Point(6, 154);
            this.check15.Name = "check15";
            this.check15.Size = new System.Drawing.Size(405, 24);
            this.check15.TabIndex = 8;
            this.check15.Text = "Urformen aus dem span- oder faserförmigen Zustand";
            this.check15.UseVisualStyleBackColor = true;
            // 
            // check16
            // 
            this.check16.AutoSize = true;
            this.check16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check16.Location = new System.Drawing.Point(6, 184);
            this.check16.Name = "check16";
            this.check16.Size = new System.Drawing.Size(405, 24);
            this.check16.TabIndex = 9;
            this.check16.Text = "Urformen aus dem gas- oder dampfförmigen Zustand";
            this.check16.UseVisualStyleBackColor = true;
            // 
            // check17
            // 
            this.check17.AutoSize = true;
            this.check17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check17.Location = new System.Drawing.Point(6, 214);
            this.check17.Name = "check17";
            this.check17.Size = new System.Drawing.Size(299, 24);
            this.check17.TabIndex = 10;
            this.check17.Text = "Urformen aus dem ionisierten Zustand";
            this.check17.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.check67);
            this.panel1.Controls.Add(this.check66);
            this.panel1.Controls.Add(this.check65);
            this.panel1.Controls.Add(this.check64);
            this.panel1.Controls.Add(this.check63);
            this.panel1.Controls.Add(this.check62);
            this.panel1.Controls.Add(this.check61);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.check58);
            this.panel1.Controls.Add(this.check57);
            this.panel1.Controls.Add(this.check56);
            this.panel1.Controls.Add(this.check55);
            this.panel1.Controls.Add(this.check54);
            this.panel1.Controls.Add(this.check53);
            this.panel1.Controls.Add(this.check52);
            this.panel1.Controls.Add(this.check51);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.check45);
            this.panel1.Controls.Add(this.check44);
            this.panel1.Controls.Add(this.check43);
            this.panel1.Controls.Add(this.check42);
            this.panel1.Controls.Add(this.check41);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.check38);
            this.panel1.Controls.Add(this.check37);
            this.panel1.Controls.Add(this.check36);
            this.panel1.Controls.Add(this.check35);
            this.panel1.Controls.Add(this.check34);
            this.panel1.Controls.Add(this.check33);
            this.panel1.Controls.Add(this.check32);
            this.panel1.Controls.Add(this.check31);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.check26);
            this.panel1.Controls.Add(this.check25);
            this.panel1.Controls.Add(this.check24);
            this.panel1.Controls.Add(this.check23);
            this.panel1.Controls.Add(this.check22);
            this.panel1.Controls.Add(this.check21);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.check17);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.check16);
            this.panel1.Controls.Add(this.check11);
            this.panel1.Controls.Add(this.check15);
            this.panel1.Controls.Add(this.check12);
            this.panel1.Controls.Add(this.check14);
            this.panel1.Controls.Add(this.check13);
            this.panel1.Location = new System.Drawing.Point(59, 111);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(804, 486);
            this.panel1.TabIndex = 12;
            // 
            // check67
            // 
            this.check67.AutoSize = true;
            this.check67.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check67.Location = new System.Drawing.Point(459, 662);
            this.check67.Name = "check67";
            this.check67.Size = new System.Drawing.Size(221, 24);
            this.check67.TabIndex = 49;
            this.check67.Text = "Photochemische Verfahren";
            this.check67.UseVisualStyleBackColor = true;
            // 
            // check66
            // 
            this.check66.AutoSize = true;
            this.check66.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check66.Location = new System.Drawing.Point(459, 632);
            this.check66.Name = "check66";
            this.check66.Size = new System.Drawing.Size(105, 24);
            this.check66.TabIndex = 48;
            this.check66.Text = "Bestrahlen";
            this.check66.UseVisualStyleBackColor = true;
            // 
            // check65
            // 
            this.check65.AutoSize = true;
            this.check65.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check65.Location = new System.Drawing.Point(459, 602);
            this.check65.Name = "check65";
            this.check65.Size = new System.Drawing.Size(128, 24);
            this.check65.TabIndex = 47;
            this.check65.Text = "Magnetisieren";
            this.check65.UseVisualStyleBackColor = true;
            // 
            // check64
            // 
            this.check64.AutoSize = true;
            this.check64.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check64.Location = new System.Drawing.Point(459, 572);
            this.check64.Name = "check64";
            this.check64.Size = new System.Drawing.Size(148, 24);
            this.check64.TabIndex = 46;
            this.check64.Text = "Sintern, Brennen";
            this.check64.UseVisualStyleBackColor = true;
            // 
            // check63
            // 
            this.check63.AutoSize = true;
            this.check63.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check63.Location = new System.Drawing.Point(459, 542);
            this.check63.Name = "check63";
            this.check63.Size = new System.Drawing.Size(265, 24);
            this.check63.TabIndex = 45;
            this.check63.Text = "Thermomechanisches Behandeln";
            this.check63.UseVisualStyleBackColor = true;
            // 
            // check62
            // 
            this.check62.AutoSize = true;
            this.check62.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check62.Location = new System.Drawing.Point(459, 512);
            this.check62.Name = "check62";
            this.check62.Size = new System.Drawing.Size(145, 24);
            this.check62.TabIndex = 44;
            this.check62.Text = "Wärmebehandel";
            this.check62.UseVisualStyleBackColor = true;
            // 
            // check61
            // 
            this.check61.AutoSize = true;
            this.check61.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check61.Location = new System.Drawing.Point(459, 482);
            this.check61.Name = "check61";
            this.check61.Size = new System.Drawing.Size(233, 24);
            this.check61.TabIndex = 43;
            this.check61.Text = "Verfestigen durch Umformen";
            this.check61.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(454, 450);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(292, 29);
            this.label7.TabIndex = 42;
            this.label7.Text = "Stoffeigenschaften ändern";
            // 
            // check58
            // 
            this.check58.AutoSize = true;
            this.check58.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check58.Location = new System.Drawing.Point(459, 423);
            this.check58.Name = "check58";
            this.check58.Size = new System.Drawing.Size(77, 24);
            this.check58.TabIndex = 41;
            this.check58.Text = "Kleben";
            this.check58.UseVisualStyleBackColor = true;
            // 
            // check57
            // 
            this.check57.AutoSize = true;
            this.check57.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check57.Location = new System.Drawing.Point(459, 393);
            this.check57.Name = "check57";
            this.check57.Size = new System.Drawing.Size(167, 24);
            this.check57.TabIndex = 40;
            this.check57.Text = "Fügen durch Löten ";
            this.check57.UseVisualStyleBackColor = true;
            // 
            // check56
            // 
            this.check56.AutoSize = true;
            this.check56.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check56.Location = new System.Drawing.Point(459, 364);
            this.check56.Name = "check56";
            this.check56.Size = new System.Drawing.Size(205, 24);
            this.check56.TabIndex = 39;
            this.check56.Text = "Fügen durch Schweißen ";
            this.check56.UseVisualStyleBackColor = true;
            // 
            // check55
            // 
            this.check55.AutoSize = true;
            this.check55.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check55.Location = new System.Drawing.Point(459, 334);
            this.check55.Name = "check55";
            this.check55.Size = new System.Drawing.Size(197, 24);
            this.check55.TabIndex = 38;
            this.check55.Text = "Fügen durch Umformen";
            this.check55.UseVisualStyleBackColor = true;
            // 
            // check54
            // 
            this.check54.AutoSize = true;
            this.check54.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check54.Location = new System.Drawing.Point(459, 304);
            this.check54.Name = "check54";
            this.check54.Size = new System.Drawing.Size(193, 24);
            this.check54.TabIndex = 37;
            this.check54.Text = "Fügen durch Urformen ";
            this.check54.UseVisualStyleBackColor = true;
            // 
            // check53
            // 
            this.check53.AutoSize = true;
            this.check53.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check53.Location = new System.Drawing.Point(459, 274);
            this.check53.Name = "check53";
            this.check53.Size = new System.Drawing.Size(172, 24);
            this.check53.TabIndex = 36;
            this.check53.Text = "An- und Einpressen ";
            this.check53.UseVisualStyleBackColor = true;
            // 
            // check52
            // 
            this.check52.AutoSize = true;
            this.check52.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check52.Location = new System.Drawing.Point(459, 244);
            this.check52.Name = "check52";
            this.check52.Size = new System.Drawing.Size(71, 24);
            this.check52.TabIndex = 35;
            this.check52.Text = "Füllen";
            this.check52.UseVisualStyleBackColor = true;
            // 
            // check51
            // 
            this.check51.AutoSize = true;
            this.check51.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check51.Location = new System.Drawing.Point(459, 214);
            this.check51.Name = "check51";
            this.check51.Size = new System.Drawing.Size(156, 24);
            this.check51.TabIndex = 34;
            this.check51.Text = "Zusammensetzen";
            this.check51.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(454, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 29);
            this.label6.TabIndex = 33;
            this.label6.Text = "Fügen";
            // 
            // check45
            // 
            this.check45.AutoSize = true;
            this.check45.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check45.Location = new System.Drawing.Point(459, 154);
            this.check45.Name = "check45";
            this.check45.Size = new System.Drawing.Size(146, 24);
            this.check45.TabIndex = 32;
            this.check45.Text = "Schubumformen";
            this.check45.UseVisualStyleBackColor = true;
            // 
            // check44
            // 
            this.check44.AutoSize = true;
            this.check44.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check44.Location = new System.Drawing.Point(459, 124);
            this.check44.Name = "check44";
            this.check44.Size = new System.Drawing.Size(141, 24);
            this.check44.TabIndex = 31;
            this.check44.Text = "Biegeumformen";
            this.check44.UseVisualStyleBackColor = true;
            // 
            // check43
            // 
            this.check43.AutoSize = true;
            this.check43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check43.Location = new System.Drawing.Point(459, 94);
            this.check43.Name = "check43";
            this.check43.Size = new System.Drawing.Size(128, 24);
            this.check43.TabIndex = 30;
            this.check43.Text = "Zugumformen";
            this.check43.UseVisualStyleBackColor = true;
            // 
            // check42
            // 
            this.check42.AutoSize = true;
            this.check42.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check42.Location = new System.Drawing.Point(459, 64);
            this.check42.Name = "check42";
            this.check42.Size = new System.Drawing.Size(167, 24);
            this.check42.TabIndex = 29;
            this.check42.Text = "Zugdruckumformen";
            this.check42.UseVisualStyleBackColor = true;
            // 
            // check41
            // 
            this.check41.AutoSize = true;
            this.check41.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check41.Location = new System.Drawing.Point(459, 34);
            this.check41.Name = "check41";
            this.check41.Size = new System.Drawing.Size(142, 24);
            this.check41.TabIndex = 28;
            this.check41.Text = "Druckumformen";
            this.check41.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(454, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 29);
            this.label5.TabIndex = 27;
            this.label5.Text = "Umformen";
            // 
            // check38
            // 
            this.check38.AutoSize = true;
            this.check38.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check38.Location = new System.Drawing.Point(6, 692);
            this.check38.Name = "check38";
            this.check38.Size = new System.Drawing.Size(320, 24);
            this.check38.TabIndex = 26;
            this.check38.Text = "Beschichten aus dem ionisierten Zustand";
            this.check38.UseVisualStyleBackColor = true;
            // 
            // check37
            // 
            this.check37.AutoSize = true;
            this.check37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check37.Location = new System.Drawing.Point(6, 662);
            this.check37.Name = "check37";
            this.check37.Size = new System.Drawing.Size(430, 24);
            this.check37.TabIndex = 25;
            this.check37.Text = "Beschichten aus dem gas- oder dampfförmigen Zustand ";
            this.check37.UseVisualStyleBackColor = true;
            // 
            // check36
            // 
            this.check36.AutoSize = true;
            this.check36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check36.Location = new System.Drawing.Point(6, 632);
            this.check36.Name = "check36";
            this.check36.Size = new System.Drawing.Size(205, 24);
            this.check36.TabIndex = 24;
            this.check36.Text = "Beschichten durch Löten";
            this.check36.UseVisualStyleBackColor = true;
            // 
            // check35
            // 
            this.check35.AutoSize = true;
            this.check35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check35.Location = new System.Drawing.Point(6, 602);
            this.check35.Name = "check35";
            this.check35.Size = new System.Drawing.Size(243, 24);
            this.check35.TabIndex = 23;
            this.check35.Text = "Beschichten durch Schweißen";
            this.check35.UseVisualStyleBackColor = true;
            // 
            // check34
            // 
            this.check34.AutoSize = true;
            this.check34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check34.Location = new System.Drawing.Point(6, 572);
            this.check34.Name = "check34";
            this.check34.Size = new System.Drawing.Size(453, 24);
            this.check34.TabIndex = 22;
            this.check34.Text = "Beschichten aus dem körnigen oder pulverförmigen Zustand";
            this.check34.UseVisualStyleBackColor = true;
            // 
            // check33
            // 
            this.check33.AutoSize = true;
            this.check33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check33.Location = new System.Drawing.Point(6, 542);
            this.check33.Name = "check33";
            this.check33.Size = new System.Drawing.Size(304, 24);
            this.check33.TabIndex = 21;
            this.check33.Text = "Beschichten aus dem breiigen Zustand";
            this.check33.UseVisualStyleBackColor = true;
            // 
            // check32
            // 
            this.check32.AutoSize = true;
            this.check32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check32.Location = new System.Drawing.Point(6, 512);
            this.check32.Name = "check32";
            this.check32.Size = new System.Drawing.Size(328, 24);
            this.check32.TabIndex = 20;
            this.check32.Text = "Beschichten aus dem plastischen Zustand";
            this.check32.UseVisualStyleBackColor = true;
            // 
            // check31
            // 
            this.check31.AutoSize = true;
            this.check31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check31.Location = new System.Drawing.Point(6, 482);
            this.check31.Name = "check31";
            this.check31.Size = new System.Drawing.Size(311, 24);
            this.check31.TabIndex = 19;
            this.check31.Text = "Beschichten aus dem flüssigen Zustand";
            this.check31.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 450);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 29);
            this.label4.TabIndex = 18;
            this.label4.Text = "Beschichten";
            // 
            // check26
            // 
            this.check26.AutoSize = true;
            this.check26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check26.Location = new System.Drawing.Point(6, 423);
            this.check26.Name = "check26";
            this.check26.Size = new System.Drawing.Size(242, 24);
            this.check26.TabIndex = 17;
            this.check26.Text = "Reinigen (Reinigungsstrahlen)";
            this.check26.UseVisualStyleBackColor = true;
            // 
            // check25
            // 
            this.check25.AutoSize = true;
            this.check25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check25.Location = new System.Drawing.Point(6, 393);
            this.check25.Name = "check25";
            this.check25.Size = new System.Drawing.Size(91, 24);
            this.check25.TabIndex = 16;
            this.check25.Text = "Zerlegen";
            this.check25.UseVisualStyleBackColor = true;
            // 
            // check24
            // 
            this.check24.AutoSize = true;
            this.check24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check24.Location = new System.Drawing.Point(6, 363);
            this.check24.Name = "check24";
            this.check24.Size = new System.Drawing.Size(94, 24);
            this.check24.TabIndex = 15;
            this.check24.Text = "Abtragen";
            this.check24.UseVisualStyleBackColor = true;
            // 
            // check23
            // 
            this.check23.AutoSize = true;
            this.check23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check23.Location = new System.Drawing.Point(6, 333);
            this.check23.Name = "check23";
            this.check23.Size = new System.Drawing.Size(385, 24);
            this.check23.TabIndex = 14;
            this.check23.Text = "Spanen mit geometrisch unbestimmten Schneiden";
            this.check23.UseVisualStyleBackColor = true;
            // 
            // check22
            // 
            this.check22.AutoSize = true;
            this.check22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check22.Location = new System.Drawing.Point(6, 303);
            this.check22.Name = "check22";
            this.check22.Size = new System.Drawing.Size(367, 24);
            this.check22.TabIndex = 13;
            this.check22.Text = "Spanen mit geometrisch bestimmten Schneiden";
            this.check22.UseVisualStyleBackColor = true;
            // 
            // check21
            // 
            this.check21.AutoSize = true;
            this.check21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.check21.Location = new System.Drawing.Point(6, 273);
            this.check21.Name = "check21";
            this.check21.Size = new System.Drawing.Size(90, 24);
            this.check21.TabIndex = 12;
            this.check21.Text = "Zerteilen";
            this.check21.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 241);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 29);
            this.label3.TabIndex = 11;
            this.label3.Text = "Trennen";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(59, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(278, 46);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(445, 603);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(75, 23);
            this.btnFilter.TabIndex = 14;
            this.btnFilter.Text = "Filtern";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.BtnFilter_Click);
            // 
            // progressFilter
            // 
            this.progressFilter.Location = new System.Drawing.Point(325, 632);
            this.progressFilter.Name = "progressFilter";
            this.progressFilter.Size = new System.Drawing.Size(315, 23);
            this.progressFilter.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressFilter.TabIndex = 15;
            // 
            // listBox
            // 
            this.listBox.FormattingEnabled = true;
            this.listBox.Location = new System.Drawing.Point(890, 85);
            this.listBox.Name = "listBox";
            this.listBox.Size = new System.Drawing.Size(211, 511);
            this.listBox.TabIndex = 16;
            this.listBox.SelectedIndexChanged += new System.EventHandler(this.ListBox_SelectedIndexChanged);
            // 
            // listHits
            // 
            this.listHits.FormattingEnabled = true;
            this.listHits.Location = new System.Drawing.Point(1107, 85);
            this.listHits.Name = "listHits";
            this.listHits.Size = new System.Drawing.Size(114, 511);
            this.listHits.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(887, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 17);
            this.label8.TabIndex = 50;
            this.label8.Text = "Lieferant";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1103, 64);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(118, 17);
            this.label9.TabIndex = 51;
            this.label9.Text = "Übereinstimmung";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1235, 673);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.listHits);
            this.Controls.Add(this.listBox);
            this.Controls.Add(this.progressFilter);
            this.Controls.Add(this.btnFilter);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFileName);
            this.Controls.Add(this.btnFolder);
            this.Name = "Form1";
            this.Text = "Lieferanten Filter";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFolder;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox check11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox check12;
        private System.Windows.Forms.CheckBox check13;
        private System.Windows.Forms.CheckBox check14;
        private System.Windows.Forms.CheckBox check15;
        private System.Windows.Forms.CheckBox check16;
        private System.Windows.Forms.CheckBox check17;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox check26;
        private System.Windows.Forms.CheckBox check25;
        private System.Windows.Forms.CheckBox check24;
        private System.Windows.Forms.CheckBox check23;
        private System.Windows.Forms.CheckBox check22;
        private System.Windows.Forms.CheckBox check21;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox check38;
        private System.Windows.Forms.CheckBox check37;
        private System.Windows.Forms.CheckBox check36;
        private System.Windows.Forms.CheckBox check35;
        private System.Windows.Forms.CheckBox check34;
        private System.Windows.Forms.CheckBox check33;
        private System.Windows.Forms.CheckBox check32;
        private System.Windows.Forms.CheckBox check31;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox check41;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox check45;
        private System.Windows.Forms.CheckBox check44;
        private System.Windows.Forms.CheckBox check43;
        private System.Windows.Forms.CheckBox check42;
        private System.Windows.Forms.CheckBox check67;
        private System.Windows.Forms.CheckBox check66;
        private System.Windows.Forms.CheckBox check65;
        private System.Windows.Forms.CheckBox check64;
        private System.Windows.Forms.CheckBox check63;
        private System.Windows.Forms.CheckBox check62;
        private System.Windows.Forms.CheckBox check61;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox check58;
        private System.Windows.Forms.CheckBox check57;
        private System.Windows.Forms.CheckBox check56;
        private System.Windows.Forms.CheckBox check55;
        private System.Windows.Forms.CheckBox check54;
        private System.Windows.Forms.CheckBox check53;
        private System.Windows.Forms.CheckBox check52;
        private System.Windows.Forms.CheckBox check51;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.ProgressBar progressFilter;
        private System.Windows.Forms.ListBox listBox;
        private System.Windows.Forms.ListBox listHits;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}

