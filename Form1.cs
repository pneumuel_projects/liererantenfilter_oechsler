﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop;
using System.Reflection;
using System.IO;
using System.Text.RegularExpressions;

namespace Angebotsliste
{
    public partial class Form1 : Form
    {

        FolderBrowserDialog fbd = new FolderBrowserDialog();
        List<Lieferant> lieferants = new List<Lieferant>() { };
        string[] files;

        public Form1()
        {
            InitializeComponent();
        }

        private void BtnFolder_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    
                    try
                    {
                        files = Directory.GetFiles(fbd.SelectedPath, "*.xlsx", SearchOption.AllDirectories);
                        //files = Directory.GetFiles(fbd.SelectedPath);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Directory / FileGet " + ex);
                    }
                    if (files.Length > 20)
                    {
                        DialogResult demo = MessageBox.Show("Demo Anwendung lässt nur 20 Excel Dateien zu! Für mehr bitte die Vollverion erwerben", "caption", MessageBoxButtons.OK);
                        if (result == DialogResult.OK)
                        {
                            System.Windows.Forms.Application.Exit();
                        }
                    }
                    System.Windows.Forms.MessageBox.Show("Anzahl Angebote " + files.Length.ToString(), "Message");
                    txtFileName.Text = fbd.SelectedPath;
                }
                progressFilter.Maximum = files.Length;
            }
            catch (NullReferenceException ne)
            {
                System.Windows.Forms.MessageBox.Show("Bitte einen Dateipfad angeben!", "Message");
            }
           
        }

        private void BtnFilter_Click(object sender, EventArgs e)
        {
            if(txtFileName.Text == "")
            {
                System.Windows.Forms.MessageBox.Show("Kein Dateipfad angegeben!", "Message");
                return;
            }
            progressFilter.Value = 0;
            lieferants.Clear();

            for (int i = 0; i < files.Length; i++)
            {
                progressFilter.Value += 1;
                lieferants.Add(new Lieferant());
                Excel excel = new Excel(files[i], 1);
                lieferants[i].Name = excel.ReadCell(12, 2);
                lieferants[i].Filename = files[i];

                #region

                //first
                if (excel.ReadCell(22 - 1, 5 - 1) == "x" || excel.ReadCell(22 - 1, 4 - 1) == "X")
                {
                    if (check11.Checked)
                    {
                        lieferants[i].Check11 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(23 - 1, 5 - 1) == "x" || excel.ReadCell(23 - 1, 5 - 1) == "X")
                {
                    if (check12.Checked)
                    {
                        lieferants[i].Check12 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(24 - 1, 5 - 1) == "x" || excel.ReadCell(24 - 1, 5 - 1) == "X")
                {
                    if (check13.Checked)
                    {
                        lieferants[i].Check13 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(25 - 1, 5 - 1) == "x" || excel.ReadCell(25 - 1, 5 - 1) == "X")
                {
                    if (check14.Checked)
                    {
                        lieferants[i].Check14 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(26 - 1, 5 - 1) == "x" || excel.ReadCell(26 - 1, 5 - 1) == "X")
                {
                    if (check15.Checked)
                    {
                        lieferants[i].Check15 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(27 - 1, 5 - 1) == "x" || excel.ReadCell(27 - 1, 5 - 1) == "X")
                {
                    if (check16.Checked)
                    {
                        lieferants[i].Check16 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(28 - 1, 5 - 1) == "x" || excel.ReadCell(28 - 1, 5 - 1) == "X")
                {
                    if (check17.Checked)
                    {
                        lieferants[i].Check17 = true;
                        lieferants[i].Hits++;
                    }
                }

                //second
                if (excel.ReadCell(31 - 1, 5 - 1) == "x" || excel.ReadCell(31 - 1, 5 - 1) == "X")
                {
                    if (check21.Checked)
                    {
                        lieferants[i].Check21 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(32 - 1, 5 - 1) == "x" || excel.ReadCell(32 - 1, 5 - 1) == "X")
                {
                    if (check22.Checked)
                    {
                        lieferants[i].Check22 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(33 - 1, 5 - 1) == "x" || excel.ReadCell(33 - 1, 5 - 1) == "X")
                {
                    if (check23.Checked)
                    {
                        lieferants[i].Check23 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(34 - 1, 5 - 1) == "x" || excel.ReadCell(34 - 1, 5 - 1) == "X")
                {
                    if (check24.Checked)
                    {
                        lieferants[i].Check24 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(35 - 1, 5 - 1) == "x" || excel.ReadCell(35 - 1, 5 - 1) == "X")
                {
                    if (check25.Checked)
                    {
                        lieferants[i].Check25 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(36 - 1, 5 - 1) == "x" || excel.ReadCell(36 - 1, 5 - 1) == "X")
                {
                    if (check26.Checked)
                    {
                        lieferants[i].Check26 = true;
                        lieferants[i].Hits++;
                    }
                }

                //third
                if (excel.ReadCell(39 - 1, 5 - 1) == "x" || excel.ReadCell(39 - 1, 5 - 1) == "X")
                {
                    if (check31.Checked)
                    {
                        lieferants[i].Check31 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(40 - 1, 5 - 1) == "x" || excel.ReadCell(40 - 1, 5 - 1) == "X")
                {
                    if (check32.Checked)
                    {
                        lieferants[i].Check32 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(41 - 1, 5 - 1) == "x" || excel.ReadCell(41 - 1, 5 - 1) == "X")
                {
                    if (check33.Checked)
                    {
                        lieferants[i].Check33 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(42 - 1, 5 - 1) == "x" || excel.ReadCell(42 - 1, 5 - 1) == "X")
                {
                    if (check34.Checked)
                    {
                        lieferants[i].Check34 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(43 - 1, 5 - 1) == "x" || excel.ReadCell(43 - 1, 5 - 1) == "X")
                {
                    if (check35.Checked)
                    {
                        lieferants[i].Check35 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(44 - 1, 5 - 1) == "x" || excel.ReadCell(44 - 1, 5 - 1) == "X")
                {
                    if (check36.Checked)
                    {
                        lieferants[i].Check36 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(45 - 1, 5 - 1) == "x" || excel.ReadCell(45 - 1, 5 - 1) == "X")
                {
                    if (check37.Checked)
                    {
                        lieferants[i].Check37 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(46 - 1, 5 - 1) == "x" || excel.ReadCell(46 - 1, 5 - 1) == "X")
                {
                    if (check38.Checked)
                    {
                        lieferants[i].Check38 = true;
                        lieferants[i].Hits++;
                    }
                }

                //fourt
                if (excel.ReadCell(22 - 1, 10 - 1) == "x" || excel.ReadCell(22 - 1, 10 - 1) == "X")
                {
                    if (check41.Checked)
                    {
                        lieferants[i].Check41 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(23 - 1, 10 - 1) == "x" || excel.ReadCell(23 - 1, 10 - 1) == "X")
                {
                    if (check42.Checked)
                    {
                        lieferants[i].Check42 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(24 - 1, 10 - 1) == "x" || excel.ReadCell(24 - 1, 10 - 1) == "X")
                {
                    if (check43.Checked)
                    {
                        lieferants[i].Check43 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(25 - 1, 10 - 1) == "x" || excel.ReadCell(25 - 1, 10 - 1) == "X")
                {
                    if (check44.Checked)
                    {
                        lieferants[i].Check44 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(26 - 1, 10 - 1) == "x" || excel.ReadCell(26 - 1, 10 - 1) == "X")
                {
                    if (check45.Checked)
                    {
                        lieferants[i].Check45 = true;
                        lieferants[i].Hits++;
                    }
                }

                //five
                if (excel.ReadCell(31 - 1, 10 - 1) == "x" || excel.ReadCell(31 - 1, 10 - 1) == "X")
                {
                    if (check51.Checked)
                    {
                        lieferants[i].Check51 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(32 - 1, 10 - 1) == "x" || excel.ReadCell(32 - 1, 10 - 1) == "X")
                {
                    if (check52.Checked)
                    {
                        lieferants[i].Check52 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(33 - 1, 10 - 1) == "x" || excel.ReadCell(33 - 1, 10 - 1) == "X")
                {
                    if (check53.Checked)
                    {
                        lieferants[i].Check53 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(34 - 1, 10 - 1) == "x" || excel.ReadCell(34 - 1, 10 - 1) == "X")
                {
                    if (check54.Checked)
                    {
                        lieferants[i].Check54 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(35 - 1, 10 - 1) == "x" || excel.ReadCell(35 - 1, 10 - 1) == "X")
                {
                    if (check55.Checked)
                    {
                        lieferants[i].Check55 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(36 - 1, 10 - 1) == "x" || excel.ReadCell(36 - 1, 10 - 1) == "X")
                {
                    if (check56.Checked)
                    {
                        lieferants[i].Check56 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(37 - 1, 10 - 1) == "x" || excel.ReadCell(37 - 1, 10 - 1) == "X")
                {
                    if (check57.Checked)
                    {
                        lieferants[i].Check57 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(38 - 1, 10 - 1) == "x" || excel.ReadCell(38 - 1, 10 - 1) == "X")
                {
                    if (check58.Checked)
                    {
                        lieferants[i].Check58 = true;
                        lieferants[i].Hits++;
                    }
                }

                //six
                if (excel.ReadCell(41 - 1, 10 - 1) == "x" || excel.ReadCell(41 - 1, 10 - 1) == "X")
                {
                    if (check61.Checked)
                    {
                        lieferants[i].Check61 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(42 - 1, 10 - 1) == "x" || excel.ReadCell(42 - 1, 10 - 1) == "X")
                {
                    if (check62.Checked)
                    {
                        lieferants[i].Check62 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(43 - 1, 10 - 1) == "x" || excel.ReadCell(43 - 1, 10 - 1) == "X")
                {
                    if (check63.Checked)
                    {
                        lieferants[i].Check63 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(44 - 1, 10 - 1) == "x" || excel.ReadCell(44 - 1, 10 - 1) == "X")
                {
                    if (check64.Checked)
                    {
                        lieferants[i].Check64 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(45 - 1, 10 - 1) == "x" || excel.ReadCell(45 - 1, 10 - 1) == "X")
                {
                    if (check65.Checked)
                    {
                        lieferants[i].Check65 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(46 - 1, 10 - 1) == "x" || excel.ReadCell(46 - 1, 10 - 1) == "X")
                {
                    if (check66.Checked)
                    {
                        lieferants[i].Check66 = true;
                        lieferants[i].Hits++;
                    }
                }
                if (excel.ReadCell(47 - 1, 10 - 1) == "x" || excel.ReadCell(47 - 1, 10 - 1) == "X")
                {
                    if (check67.Checked)
                    {
                        lieferants[i].Check67 = true;
                        lieferants[i].Hits++;
                    }
                }

                #endregion
                excel.CloseFile();
                
            }
            ListView();
        }

        private void ListView()
        {
            listBox.Items.Clear();
            listHits.Items.Clear();
            var ordered = lieferants.OrderBy(f => f.Hits);
            for (int i = 0; i < files.Length - 1; i++)
            {
                listBox.Items.Add(ordered.ElementAt((files.Length - i) - 1).Name);
                listHits.Items.Add(ordered.ElementAt((files.Length - i) - 1).Hits);
            }

        }

        private void ListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string curItem = listBox.SelectedItem.ToString();
            Excel excel = new Excel();


            for (int i = 0; i < files.Length; i++)
            {
                string normalized1 = Regex.Replace(curItem, @"\s", "");
                string normalized2 = Regex.Replace(lieferants[i].Name, @"\s", "");

                Console.WriteLine(normalized1 + " " + normalized2);

                bool stringEquals = String.Equals(normalized1, normalized2, StringComparison.OrdinalIgnoreCase);
                Console.WriteLine(stringEquals);
                if (stringEquals)
                {
                    excel.OpenFile(lieferants[i].Filename);
                }
            }
        }
    }
}
